﻿using Unity.Mathematics;
using UnityEngine;

public static class NativeVectorExtentions
{
	public static float2 AsNative(this Vector2 source) => new float2(source.x, source.y);
	public static Vector2 AsVector(this float2 source) => new Vector2(source.x, source.y);

	public static float3 AsNative(this Vector3 source) => new float3(source.x, source.y, source.z);
	public static Vector3 AsVector(this float3 source) => new Vector3(source.x, source.y, source.z);

	public static float4 AsNative(this Vector4 source) => new float4(source.x, source.y, source.z, source.w);
	public static Vector4 AsVector(this float4 source) => new Vector4(source.x, source.y, source.z, source.w);

	public static int2 AsNative(this Vector2Int source) => new int2(source.x, source.y);
	public static Vector2Int AsVector(this int2 source) => new Vector2Int(source.x, source.y);

	public static int3 AsNative(this Vector3Int source) => new int3(source.x, source.y, source.z);
	public static Vector3Int AsVector(this int3 source) => new Vector3Int(source.x, source.y, source.z);
}
