﻿using System;
using Unity.Entities;
using UnityEngine;

[Serializable]
public struct MovementSystemConfig : ISharedComponentData
{
	[SerializeField] private float duration;
	public float Duration => duration;
}
