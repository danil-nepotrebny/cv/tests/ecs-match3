﻿using Unity.Entities;
using Unity.Transforms;

public class MovementSystem : ComponentSystem
{
	public bool IsAnyMoved { get; private set; }

	protected override void OnUpdate() {
		IsAnyMoved = false;

		Entities.ForEach((Entity entity, ref Movement movement, ref Translation translation) =>
		{
			if (!movement.Apply(ref translation, Time.ElapsedTime)) {
				PostUpdateCommands.RemoveComponent<Movement>(entity);
				IsAnyMoved |= false;
			}
			else {
				IsAnyMoved |= true;
			}
		});
	}

}