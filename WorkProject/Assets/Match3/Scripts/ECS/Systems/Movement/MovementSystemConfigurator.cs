﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class MovementSystemConfigurator : MonoBehaviour
{
	[SerializeField] private MovementSystemConfig Config;

	private void Update() {
		World world = World.DefaultGameObjectInjectionWorld;
		if (world != null) {
			EntityQuery query = world.EntityManager.CreateEntityQuery(typeof(ChipTag), typeof(Translation));
			world.EntityManager.AddSharedComponentData(query, Config);
		}
	}
}
