﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class SwipeInputSystem : ComponentSystem
{
	Vector2 startInput = default;
	Vector2 endInput = default;

	GridCell startCell = default;
	GridCell endCell = default;

	Translation start = default;
	Translation end = default;

	protected override void OnUpdate() {
		if (World.GetExistingSystem<MovementSystem>().IsAnyMoved) {
			return;
		}

		HandleMouseDown();
		HandleMouseUp();
	}

	private void HandleMouseUp() {
		if (Input.GetMouseButtonUp(0)) {
			ReadSwipeEnd();
			AddMovements();
			ResetSwipe();
		}
	}

	private void ReadSwipeEnd() {
		endInput = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector3 dragDirection = (endInput - startInput).normalized;
		int3 endDirecetion = (int3)(dragDirection.AsNative() * 1.4f);

		Entities.ForEach((Entity entity, ref GridCell cell, ref Translation translation) =>
		{
			if (cell.Position.AsVector() == (startCell.Position + endDirecetion).AsVector()) {
				end = translation;
			}
		});
	}

	private void ResetSwipe() {
		startCell = endCell = default;
		start = end = default;
		startInput = endInput = default;
	}

	private void AddMovements() {
		if (!IsValidSwipe) {
			return;
		}

		Entities.ForEach((Entity entity, ref ChipTag chip, ref Translation translation) =>
		{
			var moveConfig = World.EntityManager.GetSharedComponentData<MovementSystemConfig>(entity);

			if (Vector3.Distance(translation.Value.AsVector(), start.Value.AsVector()) < Mathf.Epsilon) {
				Movement movement = new Movement(start.Value, end.Value, Time.ElapsedTime, Time.ElapsedTime + moveConfig.Duration);
				PostUpdateCommands.AddComponent(entity, movement);
			}

			if (Vector3.Distance(translation.Value.AsVector(), end.Value.AsVector()) < Mathf.Epsilon) {
				Movement movement = new Movement(end.Value, start.Value, Time.ElapsedTime, Time.ElapsedTime + moveConfig.Duration);
				PostUpdateCommands.AddComponent(entity, movement);
			}
		});
	}

	private bool IsValidSwipe => IsValidStart && IsValidEnd && start.Value.AsVector() != end.Value.AsVector();
	private bool IsValidEnd => !end.Equals(default(Translation));
	private bool IsValidStart => !start.Equals(default(Translation));

	private void HandleMouseDown() {
		if (Input.GetMouseButtonDown(0)) {
			startInput = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			Entities.ForEach((Entity entity, ref GridCell cell, ref Translation translation) =>
			{
				if (cell.Contains(startInput.AsNative())) {
					startCell = cell;
					start = translation;
				}
			});
		}
	}
}
