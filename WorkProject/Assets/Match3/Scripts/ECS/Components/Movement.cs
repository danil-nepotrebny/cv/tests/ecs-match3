﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Movement : IComponentData
{
	private float3 from;
	private float3 to;

	private double startTime;
	private double endTime;

	public Movement(float3 from, float3 to, double startTime, double endTime) {
		this.from = from;
		this.to = to;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public bool Apply(ref Translation translation, double elapsedTime) {
		if (elapsedTime <= endTime && startTime != endTime) {
			double time = (elapsedTime - startTime) / (endTime - startTime);
			translation.Value = Vector3.Lerp(from.AsVector(), to.AsVector(), (float)time).AsNative();
			return true;
		}

		translation.Value = to;
		return false;
	}
}
