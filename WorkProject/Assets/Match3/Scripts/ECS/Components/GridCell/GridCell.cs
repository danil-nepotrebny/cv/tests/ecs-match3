﻿using Unity.Entities;
using System;
using Unity.Mathematics;
using UnityEngine;
using YOZH.Core.APIExtentions.DotNET;

[Serializable]
public struct GridCell : IComponentData
{
	public int3 Position;
	public float3 BoundsMin;
	public float3 BoundsMax;

	public GridCell(Grid grid, Vector3 worldPosition) {
		if (!grid) throw new ArgumentException("Invalid argument.", nameof(grid));

		Position = grid.WorldToCell(worldPosition).AsNative();
		BoundsMin = grid.GetCellCenterWorld(Position.AsVector()) - grid.cellSize / 2;
		BoundsMax = grid.GetCellCenterWorld(Position.AsVector()) + grid.cellSize / 2;
	}

	public bool Contains(float2 position) {
		bool result = true;
		result &= position.x.InRange(BoundsMin.x, BoundsMax.x);
		result &= position.y.InRange(BoundsMin.y, BoundsMax.y);
		return result;
	}
}
