﻿using UnityEngine;
using Unity.Entities;

public class GridCellAuthority : MonoBehaviour, IConvertGameObjectToEntity
{
	[SerializeField] private Grid grid;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		if (grid) {
			GridCell cell = new GridCell(grid, transform.position);
			dstManager.AddComponentData(entity, cell);
		}
	}

	private void Reset() {
		grid = GetComponentInParent<Grid>();
	}
}
