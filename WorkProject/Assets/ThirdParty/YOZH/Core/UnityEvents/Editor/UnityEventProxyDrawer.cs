﻿using UnityEditor;
using YOZH.Core.APIExtentions.Unity.Editor;

namespace YOZH.Core.UnityEvents.Editor
{
	[CustomEditor(typeof(BaseUnityEventProxy), true), CanEditMultipleObjects]
	public class UnityEventProxyBaseDrawer : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultScriptTitle(serializedObject);

			SerializedProperty outputEventProperty = serializedObject.FindProperty("Output");
			DrawPropertiesExcluding(serializedObject, EditorDrawerUtility.ScriptFieldName, outputEventProperty.name);
			EditorGUILayout.PropertyField(outputEventProperty);

			serializedObject.ApplyModifiedProperties();
		}
	}
}