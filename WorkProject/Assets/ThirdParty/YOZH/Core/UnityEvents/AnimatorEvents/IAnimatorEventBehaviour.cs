﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Core.UnityEvents.AnimatorEvents
{
	public interface IAnimatorEventBehaviour
	{
		void OnStateEnter(AnimatorStateInfo stateInfo, int layerIndex);
		void OnStateUpdate(AnimatorStateInfo stateInfo, int layerIndex);
		void OnStateExit(AnimatorStateInfo stateInfo, int layerIndex);
		void OnStateMove(AnimatorStateInfo stateInfo, int layerIndex);
		void OnStateIK(AnimatorStateInfo stateInfo, int layerIndex);
	}
}