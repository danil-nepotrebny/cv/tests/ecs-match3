﻿using UnityEngine;
using YOZH.Core.APIExtentions.DotNET;

namespace YOZH.Core.UnityEvents.AnimatorEvents
{
	public class AnimatorStateListener : StateMachineBehaviour
	{
		private IAnimatorEventBehaviour[] Behaviours;

		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GetOrCacheBehaviour(animator)) {
				Behaviours.ForEach(behaviour => behaviour.OnStateEnter(stateInfo, layerIndex));
			}
		}

		// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GetOrCacheBehaviour(animator)) {
				Behaviours.ForEach(behaviour => behaviour.OnStateUpdate(stateInfo, layerIndex));
			}
		}

		// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GetOrCacheBehaviour(animator)) {
				Behaviours.ForEach(behaviour => behaviour.OnStateExit(stateInfo, layerIndex));
			}
		}

		// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
		public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GetOrCacheBehaviour(animator)) {
				Behaviours.ForEach(behaviour => behaviour.OnStateMove(stateInfo, layerIndex));
			}
		}

		// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
		public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (GetOrCacheBehaviour(animator)) {
				Behaviours.ForEach(behaviour => behaviour.OnStateIK(stateInfo, layerIndex));
			}
		}

		private bool GetOrCacheBehaviour(Animator animator)
		{
			Behaviours = Behaviours ?? animator.GetComponents<IAnimatorEventBehaviour>();
			return Behaviours != null;
		}
	}
}