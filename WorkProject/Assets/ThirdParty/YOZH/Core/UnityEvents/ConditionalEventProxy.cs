﻿using System;
using YOZH.Core.Conditions;
using UnityEngine;
using UnityEngine.Events;
using YOZH.Core.CustomAttributes;

namespace YOZH.Core.UnityEvents
{
	public class ConditionalEventProxy : UnityEventProxy
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private MonoBehaviour ConditionBehaviour = null;

		private ICondition Condition => ConditionBehaviour as ICondition;

		public override void Invoke()
		{
			if (Condition.IsTrue()) {
				Output?.Invoke();
			}
		}
	}

	public abstract class ConditionalEventProxy<TArg> :
		UnityEventProxy<ConditionalEventProxy<TArg>.Event, TArg>
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private MonoBehaviour ConditionBehaviour = null;

		private ICondition Condition => ConditionBehaviour as ICondition;

		public override void Invoke(TArg arg)
		{
			if (Condition.IsTrue()) {
				Output?.Invoke(arg);
			}
		}

		[Serializable]
		public class Event : UnityEvent<TArg> { }
	}

	public abstract class ConditionalEventProxy<TArg0, TArg1> :
		UnityEventProxy<ConditionalEventProxy<TArg0, TArg1>.Event, TArg0, TArg1>
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private MonoBehaviour ConditionBehaviour = null;

		private ICondition Condition => ConditionBehaviour as ICondition;

		public override void Invoke(TArg0 arg0, TArg1 arg1)
		{
			if (Condition.IsTrue()) {
				Output?.Invoke(arg0, arg1);
			}
		}

		[Serializable]
		public class Event : UnityEvent<TArg0, TArg1> { }
	}

	public abstract class ConditionalEventProxy<TArg0, TArg1, TArg2> :
		UnityEventProxy<ConditionalEventProxy<TArg0, TArg1, TArg2>.Event, TArg0, TArg1, TArg2>
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private MonoBehaviour ConditionBehaviour = null;

		private ICondition Condition => ConditionBehaviour as ICondition;

		public override void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2)
		{
			if (Condition.IsTrue()) {
				Output?.Invoke(arg0, arg1, arg2);
			}
		}

		[Serializable]
		public class Event : UnityEvent<TArg0, TArg1, TArg2> { }
	}

	public abstract class ConditionalEventProxy<TArg0, TArg1, TArg2, TArg3> :
		UnityEventProxy<ConditionalEventProxy<TArg0, TArg1, TArg2, TArg3>.Event, TArg0, TArg1, TArg2, TArg3>
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private MonoBehaviour ConditionBehaviour = null;

		private ICondition Condition => ConditionBehaviour as ICondition;

		public override void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2, TArg3 arg3)
		{
			if (Condition.IsTrue()) {
				Output?.Invoke(arg0, arg1, arg2, arg3);
			}
		}

		[Serializable]
		public class Event : UnityEvent<TArg0, TArg1, TArg2, TArg3> { }
	}
}