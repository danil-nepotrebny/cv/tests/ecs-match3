﻿using System;
using UnityEngine;
using UnityEngine.Events;
using OutEvent = YOZH.Core.UnityEvents.DoubleToLongEventConverter.OutEvent;

namespace YOZH.Core.UnityEvents
{
	internal class DoubleToLongEventConverter : UnityEventConverter<double, long, DoubleToLongEventConverter.OutEvent>
	{
		[SerializeField] private OutEvent OnOutputEvent;
		[SerializeField] private MidpointRounding RoundingMode = MidpointRounding.AwayFromZero;

		protected override long ConvertArg(double arg)
		{
			return (long)Math.Round(arg, RoundingMode);
		}

		protected override OutEvent GetOnOutputEvent() => OnOutputEvent;

		[Serializable]
		public class OutEvent : UnityEvent<long> { }
	}
}