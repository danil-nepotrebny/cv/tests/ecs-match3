﻿using UnityEditor;
using YOZH.Core.APIExtentions.Unity.Editor;

namespace YOZH.Core.Conditions.Comparatos.Editor
{
	[CustomEditor(typeof(ComparatorConditionBase), editorForChildClasses: true)]
	public class ComparatorConditionEditor : UnityEditor.Editor
	{
		private const string OnTrueEventName = "OnTrueEvent";
		private const string OnFalseEventName = "OnFalseEvent";
		public override void OnInspectorGUI()
		{
			this.DrawDefaultScriptTitle(serializedObject);

			string[] excludeProps = { EditorDrawerUtility.ScriptFieldName, OnTrueEventName, OnFalseEventName };
			DrawPropertiesExcluding(serializedObject, excludeProps);

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(serializedObject.FindProperty(OnTrueEventName));
			EditorGUILayout.PropertyField(serializedObject.FindProperty(OnFalseEventName));

			serializedObject.ApplyModifiedProperties();
		}

	}
}