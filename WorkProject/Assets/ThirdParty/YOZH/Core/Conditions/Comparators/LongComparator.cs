﻿using System;
using UnityEngine;

namespace YOZH.Core.Conditions.Comparatos
{
	public class LongComparator : ComparatorCondition<long>
	{
		[SerializeField] private ComparisionMode Mode;

		protected override bool Compare(long arg1, long arg2)
		{
			switch (Mode) {
				case ComparisionMode.Equals: return arg1 == arg2;
				case ComparisionMode.NotEquals: return arg1 != arg2;

				case ComparisionMode.Less: return arg1 < arg2;
				case ComparisionMode.LessOrEqual: return arg1 <= arg2;

				case ComparisionMode.More: return arg1 > arg2;
				case ComparisionMode.MoreOrEqual: return arg1 >= arg2;

				default:
					string message = $"[{name}.{nameof(LongComparator)}] Unknown {nameof(ComparisionMode)}: {Mode}";
					throw new InvalidOperationException(message);
			}
		}

		private enum ComparisionMode
		{
			Equals,
			NotEquals,
			Less,
			LessOrEqual,
			More,
			MoreOrEqual
		}
	}
}