﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using YOZH.Core.APIExtentions.Unity;

namespace YOZH.Core.Conditions.Editor
{
	[CustomEditor(typeof(CompositeCondition), true)]
	public class CompositeConditionEditor : UnityEditor.Editor
	{
		private static List<ICondition> LoopSearch = new List<ICondition>();

		[InitializeOnLoadMethod]
		private static void SubscribeScene()
		{
			EditorSceneManager.sceneOpened += OnSceneOpened;
			EditorSceneManager.sceneSaved += OnSceneSaved;
		}

		public override void OnInspectorGUI()
		{
			SerializedProperty scriptProperty = serializedObject.FindProperty("m_Script");

			GUI.enabled = false;
			EditorGUILayout.PropertyField(scriptProperty);
			GUI.enabled = true;

			CompositeCondition targetCondition = target as CompositeCondition;
			if (IsLooped(targetCondition)) {
				EditorGUILayout.HelpBox("Looped sub-conditions detected", MessageType.Error);
			}

			DrawPropertiesExcluding(serializedObject, scriptProperty.name);
			serializedObject.ApplyModifiedProperties();
		}

		private static void OnSceneSaved(Scene scene)
		{
			ValidateConditionsOnScene(scene);
		}

		private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
		{
			ValidateConditionsOnScene(scene);
		}

		private static void ValidateConditionsOnScene(Scene scene)
		{
			List<CompositeCondition> conditions = scene.FindComponentsOfType<CompositeCondition>(true);
			foreach (CompositeCondition condition in conditions) {
				if (IsLooped(condition)) {
					Debug.LogError($"{condition.GetType().Name} on {condition.name} has looped sub conditions.");
				}
			}
		}

		private static bool IsLooped(CompositeCondition target)
		{
			LoopSearch.Clear();
			return IsLoopedRecursive(target);
		}

		private static bool IsLoopedRecursive(CompositeCondition target)
		{
			if (LoopSearch.Contains(target)) {
				return true;
			}

			if (target != null) {
				LoopSearch.Add(target);
				foreach (ICondition subTarget in target) {
					return IsLoopedRecursive(subTarget as CompositeCondition);
				}
			}

			return false;
		}
	}
}