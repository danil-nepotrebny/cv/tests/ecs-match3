﻿using System.Collections.Generic;
using System.Linq;

namespace YOZH.Core.Conditions
{
	internal sealed class ConditionOr : MultiCompositeCondition
	{
		protected override bool IsTrue(IEnumerable<ICondition> subConditions)
		{
			return subConditions.Any(item => item.IsTrue());
		}
	}
}