﻿using System;
using UnityEngine;

namespace YOZH.Core.GUIDLinks
{
	[Serializable]
	public class GameObjectGUID : GUIDLink<GameObject>
	{
		protected override GameObject getInstance()
		{
			return Resources.Load<GameObject>(loadPath);
		}
	}
}