﻿using UnityEngine;
using UnityEditor;

namespace YOZH.Core.GUIDLinks.Editor
{
	[CustomPropertyDrawer(typeof(GameObjectGUID), true)]
	public class GameObjectGUIDDrawer : GUIDLinkPropertyDrawer<GameObject>
	{
	}
}