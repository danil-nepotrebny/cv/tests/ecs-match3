﻿using UnityEngine;
using UnityEditor;
using YOZH.Core.APIExtentions.Unity.Editor;
using System.IO;

namespace YOZH.Core.GUIDLinks.Editor
{
	public abstract class GUIDLinkPropertyDrawer<T> : PropertyDrawer
	{
		protected const string GUID = "guid";
		protected const string ASSET_PATH = "assetPath";

		private float lineHeight { get { return UnityEditor.EditorGUIUtility.singleLineHeight; } }
		private float lineSpace { get { return UnityEditor.EditorGUIUtility.standardVerticalSpacing; } }

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty guid = property.FindPropertyRelative(GUID);
			SerializedProperty assetPath = property.FindPropertyRelative(ASSET_PATH);

			assetPath.stringValue = AssetDatabase.GUIDToAssetPath(guid.stringValue);

			UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(assetPath.stringValue, typeof(UnityEngine.Object));
			obj = EditorGUIUtilityExt.InterfaceField(position, getLabel(property), obj, typeof(UnityEngine.Object), typeof(T));

			if (position.Contains(Event.current.mousePosition) && Event.current.type == EventType.DragUpdated) {
				if (obj && DragAndDrop.paths.Length > 0 && GUIDLink<T>.IsCorrectAssetPath(DragAndDrop.paths[0])) {
					DragAndDrop.visualMode = DragAndDropVisualMode.Link;
				}
				else DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
			}

			string newAssetPath = AssetDatabase.GetAssetPath(obj);
			assetPath.stringValue = (GUIDLink<T>.IsCorrectAssetPath(newAssetPath)) ? newAssetPath : "";
			guid.stringValue = AssetDatabase.AssetPathToGUID(assetPath.stringValue);
		}

		protected GUIContent getLabel(SerializedProperty property)
		{
			string parentPath = Path.GetDirectoryName(property.propertyPath.Replace('.', '/')).Replace('/', '.');
			SerializedProperty parentProperty = property.serializedObject.FindProperty(parentPath);
			if (parentProperty != null && parentProperty.isArray) {
				for (int index = 0; index < parentProperty.arraySize; index++) {
					if (SerializedProperty.EqualContents(property, parentProperty.GetArrayElementAtIndex(index))) {
						return new GUIContent(string.Format("Element {0}", index));
					}
				}
			}
			return new GUIContent(property.displayName);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return UnityEditor.EditorGUIUtility.singleLineHeight * 1;
		}
	}
}