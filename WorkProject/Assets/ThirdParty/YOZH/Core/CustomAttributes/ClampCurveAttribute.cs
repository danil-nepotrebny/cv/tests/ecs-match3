﻿using System;
using UnityEngine;

namespace YOZH.Core.CustomAttributes
{
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class ClampCurveAttribute : PropertyAttribute
	{
		public Rect Range { get; private set; }

		public ClampCurveAttribute(float xMin, float yMin, float xMax, float yMax)
		{
			Range = Rect.MinMaxRect(xMin, yMin, xMax, yMax);
		}
	}
}