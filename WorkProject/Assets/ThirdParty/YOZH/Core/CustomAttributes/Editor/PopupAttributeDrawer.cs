﻿using UnityEngine;
using UnityEditor;


namespace YOZH.Core.CustomAttributes.Editor
{
	[CustomPropertyDrawer(typeof(PopupAttribute), true), CanEditMultipleObjects]
	public class PopupAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			string[] choices = ((PopupAttribute)attribute).Choices;

			if (choices.Length > 0) {
				int index = EditorGUI.Popup(position, label.text, getSelectedChoiceIndex(choices, property.stringValue), choices);
				property.stringValue = choices[index];
			}
			else {
				EditorGUI.HelpBox(position, string.Format("Please define choices for \"{0}\"", property.displayName), MessageType.Warning);
			}
		}

		private int getSelectedChoiceIndex(string[] choices, string selectedChoice)
		{
			for (int index = 0; index < choices.Length; index++) {
				if (selectedChoice == choices[index]) {
					return index;
				}
			}
			return 0;
		}
	}
}