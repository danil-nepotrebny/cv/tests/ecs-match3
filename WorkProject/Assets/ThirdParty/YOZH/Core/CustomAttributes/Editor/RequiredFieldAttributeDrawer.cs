﻿using UnityEditor;
using UnityEngine;

namespace YOZH.Core.CustomAttributes.Editor
{
	[CustomPropertyDrawer(typeof(RequiredFieldAttribute))]
	public class RequiredFieldAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			Color defaultColor = GUI.color;
			GUI.color = IsEmpty(property) ? Color.red : defaultColor;
			EditorGUI.PropertyField(position, property, label, true);
			GUI.color = defaultColor;
		}

		public bool IsEmpty(SerializedProperty property)
		{
			switch (property.propertyType) {
				case SerializedPropertyType.ObjectReference:
					return property.objectReferenceValue == null;

				case SerializedPropertyType.String:
					return string.IsNullOrEmpty(property.stringValue);

				case SerializedPropertyType.Integer:
					return property.intValue == 0;

				default:
					return false;
			}
		}

	}
}