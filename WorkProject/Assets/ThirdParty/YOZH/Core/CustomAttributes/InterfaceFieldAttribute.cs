﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Core.CustomAttributes
{
	public class InterfaceFieldAttribute : PropertyAttribute
	{
		public bool AllowSceneObject { get; private set; }
		public Type InterfaceType { get; private set; }
		public InterfaceFieldAttribute(Type interfaceType, bool allowSceneObject = false)
		{
			InterfaceType = interfaceType;
			AllowSceneObject = allowSceneObject;
		}
	}
}