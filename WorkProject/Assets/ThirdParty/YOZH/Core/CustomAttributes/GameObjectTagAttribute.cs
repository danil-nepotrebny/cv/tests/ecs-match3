﻿using UnityEngine;
using System.Collections;


namespace YOZH.Core.CustomAttributes
{
	public class GameObjectTagAttribute : PopupAttribute
	{
		public GameObjectTagAttribute()
		{
#if UNITY_EDITOR
			Choices = UnityEditorInternal.InternalEditorUtility.tags;
#endif
		}
	}
}
