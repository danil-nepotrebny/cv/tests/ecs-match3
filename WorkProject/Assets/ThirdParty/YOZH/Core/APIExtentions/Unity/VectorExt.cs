﻿using System;
using UnityEngine;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class Vector2Ext
	{
		public static Vector2 Clamp(this Vector2 target, Vector2 min, Vector2 max)
		{
			target.x = Mathf.Clamp(target.x, min.x, max.x);
			target.y = Mathf.Clamp(target.y, min.y, max.y);
			return target;
		}

		public static Vector2 Round(this Vector2 vector, int accuracy)
		{
			return new Vector2(
				(float)Math.Round(vector.x, accuracy),
				(float)Math.Round(vector.y, accuracy)
			);
		}

		public static bool IsSimilarTo(this Vector2 vector, Vector2 other)
		{
			return Mathf.Approximately(vector.x, other.x) &&
				Mathf.Approximately(vector.y, other.y);
		}

		public static bool IsPositiveOrZero(this Vector2 vector)
		{
			return vector.x >= 0 && vector.y >= 0;
		}

		public static bool IsPositiveOrZero(this Vector2Int vector)
		{
			return vector.x >= 0 && vector.y >= 0;
		}
	}

	public static class Vector3Ext
	{
		public static Vector3 Clamp(this Vector3 target, Vector3 min, Vector3 max)
		{
			target.x = Mathf.Clamp(target.x, min.x, max.x);
			target.y = Mathf.Clamp(target.y, min.y, max.y);
			target.z = Mathf.Clamp(target.z, min.z, max.z);
			return target;
		}

		public static Vector3 Round(this Vector3 vector, int accuracy)
		{
			return new Vector3(
				(float)Math.Round(vector.x, accuracy),
				(float)Math.Round(vector.y, accuracy),
				(float)Math.Round(vector.z, accuracy)
			);
		}

		public static bool IsSimilarTo(this Vector3 vector, Vector3 other)
		{
			return Mathf.Approximately(vector.x, other.x) &&
				Mathf.Approximately(vector.y, other.y) &&
				Mathf.Approximately(vector.z, other.z);
		}

		public static bool IsPositiveOrZero(this Vector3 vector)
		{
			return vector.x >= 0 && vector.y >= 0 && vector.z >= 0;
		}

		public static bool IsPositiveOrZero(this Vector3Int vector)
		{
			return vector.x >= 0 && vector.y >= 0 && vector.z >= 0;
		}
	}

	public static class Vector4Ext
	{
		public static Vector4 Clamp(this Vector4 target, Vector4 min, Vector4 max)
		{
			target.x = Mathf.Clamp(target.x, min.x, max.x);
			target.y = Mathf.Clamp(target.y, min.y, max.y);
			target.z = Mathf.Clamp(target.z, min.z, max.z);
			target.w = Mathf.Clamp(target.w, min.w, max.w);
			return target;
		}

		public static Vector4 Round(this Vector4 vector, int accuracy)
		{
			return new Vector4(
				(float)Math.Round(vector.x, accuracy),
				(float)Math.Round(vector.y, accuracy),
				(float)Math.Round(vector.z, accuracy),
				(float)Math.Round(vector.w, accuracy)
			);
		}

		public static bool IsSimilarTo(this Vector4 vector, Vector4 other)
		{
			return Mathf.Approximately(vector.x, other.x) &&
				Mathf.Approximately(vector.y, other.y) &&
				Mathf.Approximately(vector.z, other.z) &&
				Mathf.Approximately(vector.w, other.w);
		}

		public static bool IsPositiveOrZero(this Vector4 vector)
		{
			return vector.x >= 0 && vector.y >= 0 && vector.z >= 0 && vector.w >= 0;
		}
	}
}