﻿using UnityEngine;

namespace YOZH.Core.APIExtentions.Unity
{
	public struct RectTransformData
	{
		private const int RoundAccuracy = 2;

		public Vector3 Position { get; set; }
		public Quaternion Rotation { get; set; }
		public Vector3 LocalScale { get; set; }

		public Vector2 AnchorMin { get; set; }
		public Vector2 AnchorMax { get; set; }

		public Vector2 OffsetMin { get; set; }
		public Vector2 OffsetMax { get; set; }

		public RectTransformData(RectTransform original)
		{
			Position = original.position;
			Rotation = original.rotation;
			LocalScale = original.localScale;

			OffsetMin = original.offsetMin;
			OffsetMax = original.offsetMax;

			AnchorMin = original.anchorMin;
			AnchorMax = original.anchorMax;
		}

		public static RectTransformData Empty => default(RectTransformData);

		public RectTransformData Translate(Vector3 translation)
		{
			return new RectTransformData
			{
				Position = this.Position + translation,
				Rotation = this.Rotation,
				LocalScale = this.LocalScale,

				OffsetMin = this.OffsetMin,
				OffsetMax = this.OffsetMax,
				AnchorMin = this.AnchorMin,
				AnchorMax = this.AnchorMax,
			};
		}

		public void ApplyTo(RectTransform target)
		{
			target.position = Position;
			target.rotation = Rotation;
			target.localScale = LocalScale;

			target.offsetMin = OffsetMin;
			target.offsetMax = OffsetMax;

			target.anchorMin = AnchorMin;
			target.anchorMax = AnchorMax;
		}

		public RectTransformData Round(int accuracy)
		{
			return new RectTransformData()
			{
				Position = Position.Round(accuracy),
				Rotation = Rotation.Round(accuracy),
				LocalScale = LocalScale.Round(accuracy),

				OffsetMin = OffsetMin.Round(accuracy),
				OffsetMax = OffsetMax.Round(accuracy),

				AnchorMin = AnchorMin.Round(accuracy),
				AnchorMax = AnchorMax.Round(accuracy),
			};
		}

		public RectTransformData LerpTo(RectTransformData other, float time)
		{
			return new RectTransformData
			{
				Position = Vector3.Lerp(this.Position, other.Position, time),
				Rotation = Quaternion.Lerp(this.Rotation, other.Rotation, time),
				LocalScale = Vector3.Lerp(this.LocalScale, other.LocalScale, time),

				OffsetMin = Vector2.Lerp(this.OffsetMin, other.OffsetMin, time),
				OffsetMax = Vector2.Lerp(this.OffsetMax, other.OffsetMax, time),

				AnchorMin = Vector2.Lerp(this.AnchorMin, other.AnchorMin, time),
				AnchorMax = Vector2.Lerp(this.AnchorMax, other.AnchorMax, time)
			};
		}

		public bool Equals(RectTransformData other, int accuracy)
		{
			return this.Round(accuracy).Equals(other.Round(accuracy));
		}
	}
}