﻿using System.Linq;
using UnityEngine;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class AnimationCurveExt
	{
		public static float GetMinValue(this AnimationCurve curve)
		{
			return curve.keys.Min(frame => frame.value);
		}

		public static float GetMaxValue(this AnimationCurve curve)
		{
			return curve.keys.Max(frame => frame.value);
		}

		public static float GetMedian(this AnimationCurve curve)
		{
			float sum = curve.keys.Sum(frame => frame.value);
			return sum / curve.keys.Length;
		}

		public static float GetAmplitude(this AnimationCurve curve)
		{
			return Mathf.Abs(curve.GetMaxValue() - curve.GetMinValue());
		}
	}
}