﻿using UnityEngine;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class RectTransformExt
	{
		public static RectTransformData GetData(this RectTransform transform)
		{
			return new RectTransformData(transform);
		}
	}
}