﻿using UnityEditor;
using UnityEngine;
using EditorDrawer = UnityEditor.Editor;

namespace YOZH.Core.APIExtentions.Unity.Editor
{
	public static class EditorDrawerUtility
	{
		public const string ScriptFieldName = "m_Script";

		public static void DrawDefaultScriptTitle(this EditorDrawer editor, SerializedObject target)
		{
			GUI.enabled = false;
			EditorGUILayout.PropertyField(target.FindProperty(ScriptFieldName));
			GUI.enabled = true;
		}
	}
}
