﻿using System;
using UnityEditor;

namespace YOZH.Core.APIExtentions.Unity.Editor
{
	public static class SerializedPropertyExt
	{
		public static Type FieldType(this SerializedProperty property, Type fieldType)
		{
			string[] propertyPathSplit = property.propertyPath.Split('.');

			string parentPropertyPath = "";
			for (int index = 0; index < propertyPathSplit.Length - 1; index++) {
				parentPropertyPath = string.Format("{0}.{1}", parentPropertyPath, propertyPathSplit[index]);
			}
			parentPropertyPath = parentPropertyPath.TrimStart('.');

			SerializedProperty parentProperty = property.serializedObject.FindProperty(parentPropertyPath);

			if (parentProperty == null || !parentProperty.isArray) return fieldType;

			if (fieldType.IsGenericType) {
				return fieldType.GetGenericArguments()[0];
			}

			return fieldType.GetElementType();
		}

		public static string Print(this SerializedProperty property)
		{
			if (property == null) return "null";

			string result = property.ToString() + "\n{\n";

			result += string.Format("\t{0}: \"{1}\"\n", "name", property.name);
			result += string.Format("\t{0}: \"{1}\"\n", "displayName", property.displayName);
			result += "\n";

			result += string.Format("\t{0}: \"{1}\"\n", "propertyPath", property.propertyPath);
			result += string.Format("\t{0}: \"{1}\"\n", "depth", property.depth);
			result += "\n";

			result += string.Format("\t{0}: \"{1}\"\n", "type", property.type);
			result += string.Format("\t{0}: \"{1}\"\n", "propertyType", property.propertyType);

			if (property.propertyType == SerializedPropertyType.Integer) {
				result += string.Format("\t{0}: \"{1}\"\n", "intValue", property.intValue);
				result += string.Format("\t{0}: \"{1}\"\n", "longValue", property.longValue);
			}
			if (property.propertyType == SerializedPropertyType.Float) {
				result += string.Format("\t{0}: \"{1}\"\n", "floatValue", property.floatValue);
				result += string.Format("\t{0}: \"{1}\"\n", "doubleValue", property.doubleValue);
			}
			if (property.propertyType == SerializedPropertyType.Boolean) result += string.Format("\t{0}: \"{1}\"\n", "boolValue", property.boolValue);
			if (property.propertyType == SerializedPropertyType.String) result += string.Format("\t{0}: \"{1}\"\n", "stringValue", property.stringValue);
			if (property.propertyType == SerializedPropertyType.Color) result += string.Format("\t{0}: \"{1}\"\n", "colorValue", property.colorValue);
			if (property.propertyType == SerializedPropertyType.AnimationCurve) result += string.Format("\t\"{0}\": {1}\n", "animationCurveValue", property.animationCurveValue);
			if (property.propertyType == SerializedPropertyType.Vector2) result += string.Format("\t{0}: \"{1}\"\n", "vector2Value", property.vector2Value);
			if (property.propertyType == SerializedPropertyType.Vector3) result += string.Format("\t{0}: \"{1}\"\n", "vector3Value", property.vector3Value);
			if (property.propertyType == SerializedPropertyType.Vector4) result += string.Format("\t{0}: \"{1}\"\n", "vector4Value", property.vector4Value);
			if (property.propertyType == SerializedPropertyType.Bounds) result += string.Format("\t{0}: \"{1}\"\n", "boundsValue", property.boundsValue);
			if (property.propertyType == SerializedPropertyType.Rect) result += string.Format("\t{0}: \"{1}\"\n", "rectValue", property.rectValue);
			if (property.propertyType == SerializedPropertyType.Quaternion) result += string.Format("\t{0}: \"{1}\"\n", "quaternionValue", property.quaternionValue);

			if (property.propertyType == SerializedPropertyType.Enum) {
				result += string.Format("\t{0}: \"{1}\"\n", "enumValueIndex", property.enumValueIndex);
				result += string.Format("\t{0}: \"{1}\"\n", "enumNames", property.enumNames);
				result += string.Format("\t{0}: \"{1}\"\n", "enumDisplayNames", property.enumDisplayNames);
			}

			if (property.propertyType == SerializedPropertyType.ObjectReference) {
				result += string.Format("\t{0}: \"{1}\"\n", "objectReferenceValue", property.objectReferenceValue);
				result += string.Format("\t{0}: \"{1}\"\n", "objectReferenceInstanceIDValue", property.objectReferenceInstanceIDValue);
			}

			if (property.propertyType == SerializedPropertyType.ExposedReference) {
				result += string.Format("\t{0}: \"{1}\"\n", "exposedReferenceValue", property.exposedReferenceValue);
			}
			result += "\n";

			result += string.Format("\t{0}: \"{1}\"\n", "editable", property.editable);
			result += string.Format("\t{0}: \"{1}\"\n", "isInstantiatedPrefab", property.isInstantiatedPrefab);
			result += string.Format("\t{0}: \"{1}\"\n", "prefabOverride", property.prefabOverride);
			result += "\n";

			result += string.Format("\t{0}: \"{1}\"\n", "isAnimated", property.isAnimated);
			result += string.Format("\t{0}: \"{1}\"\n", "isExpanded", property.isExpanded);
			result += string.Format("\t{0}: \"{1}\"\n", "hasChildren", property.hasChildren);
			result += string.Format("\t{0}: \"{1}\"\n", "hasVisibleChildren", property.hasVisibleChildren);
			result += "\n";

			result += string.Format("\t{0}: \"{1}\"\n", "isArray", property.isArray);
			if (property.propertyType == SerializedPropertyType.ArraySize) {
				result += string.Format("\t{0}: \"{1}\"\n", "arrayElementType", property.arrayElementType);
				result += string.Format("\t{0}: \"{1}\"\n", "arraySize", property.arraySize);
			}

			result += "}";
			return result;
		}
	}
}