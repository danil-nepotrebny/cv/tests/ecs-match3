﻿using System;

namespace YOZH.Core.APIExtentions.DotNET
{
	public static class IntExt
	{
		public static void TimesRepeat(this int times, Action action)
		{
			for (int counter = 0; counter < times; counter++) {
				action.Invoke();
			}
		}

		public static void TimesRepeat(this int times, Action<int> action)
		{
			for (int counter = 0; counter < times; counter++) {
				action.Invoke(counter);
			}
		}
	}
}