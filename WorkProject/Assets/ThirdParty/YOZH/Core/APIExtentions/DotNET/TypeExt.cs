﻿using System;

namespace YOZH.Core.APIExtentions.DotNET
{
	public static class TypeExt
	{
		public static bool IsChildOf(this Type t, Type other)
		{
			return t == other || t.IsSubclassOf(other);
		}
	}
}