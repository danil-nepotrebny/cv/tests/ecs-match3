﻿using System.Collections.Generic;

namespace YOZH.Core.APIExtentions.DotNET
{
	/// <summary>
	/// Surrogate for standard .Net ReadOnlyCollection<T> class which is also implements HBG.Core.APIExtentions.DotNET.IReadOnlyList<T>
	/// </summary>
	public class ReadOnlyCollection<T> : System.Collections.ObjectModel.ReadOnlyCollection<T>, IReadOnlyList<T>
	{
		public ReadOnlyCollection(IList<T> list) : base(list)
		{
		}
	}
}