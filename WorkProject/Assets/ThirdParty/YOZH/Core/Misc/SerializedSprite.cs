﻿using System;
using System.IO;
using UnityEngine;

namespace YOZH.Core.Misc
{
	/// <summary>
	/// Я хз зачем может понадобиться этот класс...
	/// Он хранит в себе путь к спрайту и восстанавливает его через Resources.Load при этом сериализуясь хранит только стринговый путь.
	/// </summary>
	[Serializable]
	public class SerializedSprite
	{
		public string AtlasPath { get; private set; }
		public string SpriteName { get; private set; }

		[NonSerialized]
		private Sprite sprite = null;
		public Sprite Sprite {
			get {
				if (sprite == null) sprite = loadSprite();
				return sprite;
			}
			set {
				sprite = value;
				remeberAtlasPathAndSpriteName(value);
			}
		}

		private Sprite loadSprite()
		{
			Sprite result = null;
			Sprite[] atlas = Resources.LoadAll<Sprite>(AtlasPath);
			if (atlas != null) {
				foreach (Sprite s in atlas)
					if (s.name == SpriteName) result = s;
			}
			return result;
		}

		private void remeberAtlasPathAndSpriteName(Sprite sprite)
		{
#if UNITY_EDITOR
			if (sprite == null) return;

			string atlasPath = UnityEditor.AssetDatabase.GetAssetPath(sprite.texture);
			string fileNameWithoutExt = Path.GetFileNameWithoutExtension(atlasPath);
			string fileName = Path.GetFileName(atlasPath);
			string[] atlasPathList = atlasPath.Split(@"/".ToCharArray());
			atlasPath = "";
			bool gotResources = false, gotAssets = false;
			foreach (string pathPart in atlasPathList) {
				bool allowAdd = true;
				if (pathPart == "Assets" && !gotAssets) {
					gotAssets = true;
					allowAdd = false;
				}
				if (pathPart == "Resources" && !gotResources) {
					gotResources = true;
					allowAdd = false;
				}
				if (allowAdd && gotResources && gotAssets && pathPart != fileName) atlasPath += string.Format("{0}/", pathPart);
			}
			if (gotResources) {
				AtlasPath = string.Format("{0}{1}", atlasPath, fileNameWithoutExt);
				SpriteName = sprite.name;
			}
			else {
				Debug.LogError(string.Format("[{0}] Please put the atlas \"{1}\" under \"Resources\" folder.", GetType(), UnityEditor.AssetDatabase.GetAssetPath(sprite.texture)));
			}
#else
            Debug.LogError(string.Format("[{0}.remeberAtlasPathAndSpriteName({1})] This finction can't work in runtime. It is only for editor.", GetType(), sprite.name));
#endif
		}

		public SerializedSprite() { }
		public SerializedSprite(string atlasPath, string spriteName)
		{
			AtlasPath = atlasPath;
			SpriteName = spriteName;
		}
		public SerializedSprite(Sprite sprite)
		{
			Sprite = sprite;
		}

		public override string ToString()
		{
			return string.Format("{0}/{1}", AtlasPath, SpriteName);
		}

		public static explicit operator Sprite(SerializedSprite serilizedSprite)
		{
			return serilizedSprite.Sprite;
		}
	}
}