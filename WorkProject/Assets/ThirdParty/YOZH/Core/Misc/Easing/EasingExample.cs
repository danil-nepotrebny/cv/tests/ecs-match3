﻿using UnityEngine;
using System.Collections;

namespace YOZH.Core.Misc
{
	public class EasingExample : MonoBehaviour
	{
		[SerializeField]
		private Easing.Type method = Easing.Type.EaseInCirc;
		[SerializeField]
		private float speed = 1;
		[SerializeField]
		private Vector3 targetPos = new Vector3(0, 0, 0);

		IEnumerator Start()
		{
			yield return new WaitForEndOfFrame();
			Vector3 startPoss = transform.position;


			float duration = Vector3.Distance(startPoss, targetPos) / speed;
			float endTime = Time.time + duration;
			float startTime = Time.time;

			while (Time.time <= endTime) {
				transform.position = Easing.GetValue(startPoss, targetPos, startTime, duration, Time.time, method);
				yield return new WaitForEndOfFrame();
			}
			Debug.Log("Finished");
			transform.position = targetPos;

			yield break;
		}

	}
}