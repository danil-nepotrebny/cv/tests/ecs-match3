﻿using UnityEngine;
using System.Collections.Generic;


namespace YOZH.Core.Misc
{
	public static class Dice
	{
		public static float Throw(float min, float max)
		{
			return (min + ((max - min) * 0.01f * (Random.Range(0, 1000f) % 100f)));
		}

		public static int Throw(int min, int max)
		{
			return (int)Throw(min, max + 1f);
		}

		public static Vector2 Throw(Vector2 min, Vector2 max)
		{
			return new Vector2
			{
				x = Dice.Throw(min.x, max.x),
				y = Dice.Throw(min.y, max.y)
			};
		}
		public static Vector3 Throw(Vector3 min, Vector3 max)
		{
			return new Vector3
			{
				x = Dice.Throw(min.x, max.x),
				y = Dice.Throw(min.y, max.y),
				z = Dice.Throw(min.z, max.z)
			};
		}
		public static Vector4 Throw(Vector4 min, Vector4 max)
		{
			return new Vector4
			{
				x = Dice.Throw(min.x, max.x),
				y = Dice.Throw(min.y, max.y),
				z = Dice.Throw(min.z, max.z),
				w = Dice.Throw(min.w, max.w)
			};
		}

		public static float Throw()
		{
			return Throw(0, 100f);
		}

		public static int GetChoice(float[] chances)
		{
			chances = normalizeChances(chances, 100);
			float dice = Throw();
			float minChance = 0, maxChance = 0;
			for (int i = 0; i < chances.Length; i++) {
				maxChance += chances[i];

				if (i > 0) minChance += chances[i - 1];

				if (dice > minChance && dice <= maxChance) return i;
			}

			Debug.LogError(string.Format("Error with GetChoice. Dice is {0}", dice));
			return -1;
		}

		private static float[] normalizeChances(float[] array, int max)
		{
			List<float> result = new List<float>();

			float sum = 0;
			for (int i = 0; i < array.Length; i++) {
				sum += array[i];
			}

			for (int i = 0; i < array.Length; i++) {
				result.Add(array[i] * max / sum);
			}
			result.Sort();

			return result.ToArray();
		}
	}
}