﻿using UnityEngine;

namespace YOZH.Core.Misc.Interfaces
{
	public interface IGameObjectComponent
	{
		T FindComponent<T>() where T : Component;
		GameObject GameObject { get; }
	}
}
