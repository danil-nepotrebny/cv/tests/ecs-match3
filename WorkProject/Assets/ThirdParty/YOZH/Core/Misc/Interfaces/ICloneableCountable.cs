using System;

namespace YOZH.Core.Misc.Interfaces
{
	public interface ICloneableCountable : ICloneable, ICountable
	{
		object Clone(uint count);
	}
}