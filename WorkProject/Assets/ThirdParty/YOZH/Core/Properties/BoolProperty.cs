﻿using System;
using UnityEngine.Events;

namespace YOZH.Core.Properties
{
	[Serializable]
	public class BoolProperty : BaseProperty<bool, BoolProperty.ChangeEvent>
	{
		public BoolProperty() : base()
		{
		}

		public BoolProperty(bool value) : base(value)
		{
		}

		[Serializable]
		public class ChangeEvent : UnityEvent<bool, bool>
		{
		}
	}
}