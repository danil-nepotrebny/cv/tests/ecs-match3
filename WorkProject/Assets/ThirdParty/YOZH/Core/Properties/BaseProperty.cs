﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.Properties
{
	public abstract class BaseProperty<TValue, TChanged> : ICloneable
		where TValue : IConvertible
		where TChanged : UnityEvent<TValue, TValue>, new()
	{
		[SerializeField] private TValue Value = default(TValue);
		[SerializeField] private TChanged OnChanged = new TChanged();

		public event UnityAction<TValue, TValue> OnValueChanged {
			add { OnChanged.AddListener(value); }
			remove { OnChanged.RemoveListener(value); }
		}

		public BaseProperty(TValue value)
		{
			Value = value;
		}

		public BaseProperty() : this(default(TValue))
		{
		}

		public override string ToString()
		{
			return Value.ToString();
		}

		public static implicit operator TValue(BaseProperty<TValue, TChanged> property) => property.Value;

		public TValue GetValue() => Value;

		public void SetValue(TValue value)
		{
			if (!Value.Equals(value)) {
				TValue oldValue = Value;
				Value = value;
				OnChanged?.Invoke(oldValue, Value);
			}
		}

		object ICloneable.Clone()
		{
			return MemberwiseClone();
		}
	}
}