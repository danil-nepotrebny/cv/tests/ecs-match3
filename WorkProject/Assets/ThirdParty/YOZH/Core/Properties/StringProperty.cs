﻿using System;
using UnityEngine.Events;

namespace YOZH.Core.Properties
{
	[Serializable]
	public class StringProperty : BaseProperty<string, StringProperty.ChangeEvent>
	{
		public StringProperty() : base()
		{
		}

		public StringProperty(string value) : base(value)
		{
		}

		[Serializable]
		public class ChangeEvent : UnityEvent<string, string>
		{
		}
	}
}