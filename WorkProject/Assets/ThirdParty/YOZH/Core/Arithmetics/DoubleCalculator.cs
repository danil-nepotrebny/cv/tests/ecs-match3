﻿using System;
using UnityEngine;
using UnityEngine.Events;
using ResultEvent = YOZH.Core.Arithmetics.DoubleCalculator.ResultEvent;

namespace YOZH.Core.Arithmetics
{
	public class DoubleCalculator : CalculatorBehaviour<double, ResultEvent>
	{
		[SerializeField] protected ResultEvent OnResultEvent = null;

		protected override double Divide(double arg1, double arg2) => arg1 / arg2;

		protected override double Multiply(double arg1, double arg2) => arg1 * arg2;
		protected override double Substract(double arg1, double arg2) => arg1 - arg2;
		protected override double Sum(double arg1, double arg2) => arg1 + arg2;
		protected override ResultEvent GetOnResultEvent() => OnResultEvent;

		[Serializable] public class ResultEvent : UnityEvent<double> { }
	}
}