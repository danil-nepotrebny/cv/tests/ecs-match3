﻿using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.Arithmetics
{
	public abstract class CalculatorBehaviour<TArgument, TResult> : BaseCalculatorBehaviour
		where TArgument : struct
		where TResult : UnityEvent<TArgument>
	{
		[SerializeField] private TArgument Argument1 = default(TArgument);
		[SerializeField] private TArgument Argument2 = default(TArgument);
		[SerializeField] private UnityEvent OnSetArgumentsEvent = null;

		private bool IsCalculating;

		public TArgument GetArgument1() => Argument1;
		public void SetArgument1(TArgument value)
		{
			if (!Argument1.Equals(value)) {
				Argument1 = value;
				if (!IsCalculating) {
					OnSetArgumentsEvent?.Invoke();
				}
			}
		}

		public TArgument GetArgument2() => Argument2;
		public void SetArgument2(TArgument value)
		{
			if (!Argument2.Equals(value)) {
				Argument2 = value;
				if (!IsCalculating) {
					OnSetArgumentsEvent?.Invoke();
				}
			}
		}

		public void Clean()
		{
			SetArgument1(default(TArgument));
			SetArgument2(default(TArgument));
		}

		public override sealed void Sum()
		{
			IsCalculating = true;
			GetOnResultEvent()?.Invoke(Sum(Argument1, Argument2));
			IsCalculating = false;
		}

		public override sealed void Substract()
		{
			IsCalculating = true;
			GetOnResultEvent()?.Invoke(Substract(Argument1, Argument2));
			IsCalculating = false;
		}

		public override sealed void Multiply()
		{
			IsCalculating = true;
			GetOnResultEvent()?.Invoke(Multiply(Argument1, Argument2));
			IsCalculating = false;
		}

		public override sealed void Divide()
		{
			IsCalculating = true;
			GetOnResultEvent()?.Invoke(Divide(Argument1, Argument2));
			IsCalculating = false;
		}

		protected abstract TResult GetOnResultEvent();
		protected abstract TArgument Sum(TArgument arg1, TArgument arg2);
		protected abstract TArgument Substract(TArgument arg1, TArgument arg2);
		protected abstract TArgument Multiply(TArgument arg1, TArgument arg2);
		protected abstract TArgument Divide(TArgument arg1, TArgument arg2);
	}
}