﻿using UnityEngine;

namespace YOZH.Core.Arithmetics
{
	public abstract class BaseCalculatorBehaviour : MonoBehaviour
	{
		public abstract void Sum();
		public abstract void Substract();
		public abstract void Multiply();
		public abstract void Divide();
	}
}